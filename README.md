# K3d Container

Sidecar container designed to be run in tandem with [`gitlab-qa`](https://gitlab.com/gitlab-org/gitlab-qa)
to expose k3d

# Build

```bash
$ docker build -t gitlab/k3d:local .
```

# Run

```bash
$ docker run -v /var/run/docker.sock:/var/run/docker.sock -it gitlab/k3d:local bash
$ k3d --version
```

# Release

1. [Build](#build)
1. `docker tag gitlab/k3d:local gitlab/k3d:<semver>`
1. `docker push gitlab/k3d:<semver>`
